
#include "PlayingCards.h"
#include<iostream>
#include <sstream>
#include <time.h>




using namespace std;


#include <cstdio>
#include <stdlib.h>
#include <time.h>




PlayingCards::PlayingCards() :n(0)
{
}



PlayingCards::PlayingCards(const PlayingCards &obj) : n(obj.n)
{
	pack = new Card[n];
	for (int i = 0; i < n; ++i)
		pack[i] = obj.pack[i];
}


PlayingCards::PlayingCards(PlayingCards &&obj)
{
	n = obj.n;
	pack = obj.pack;
	obj.pack = nullptr;
}


PlayingCards::PlayingCards(int k) {
	srand(time(0));
	if (k > 0 && k < SZ + 1)
	{
		n = k;
		pack = new Card[n];
		for (int i = 0; i<n;) {
			int suit = rand() % 4;
			int rang = rand() % 13;
			if (findTheSame(rang, suit))
			{
				pack[i].suit = suit;
				pack[i].rang = rang;
				i++;
			}
		}
	}
	else
		throw "Illegal size";
}


PlayingCards::PlayingCards(const Card& first)
{
	if (first.suit > 3 || first.suit < 0)
		throw "Illegal Card";
	if (first.rang > 12 || first.rang < 0)
		throw "Illegal Card";
	n = 1;
	pack = new Card;
	pack[0].suit = first.suit;
	pack[0].rang = first.rang;
}




PlayingCards::~PlayingCards()
{
	if (n == 1)
		delete pack;
	else
		delete[] pack;
};



int PlayingCards::getn() const
{
	return n;
}


Card PlayingCards::getFirstCard()const
{
	return pack[0];
}


CardName PlayingCards::createPictName(int number)
{// ������ ����� ������������ ��� ��������� �������� ����� �� �� ����� � ����� , ������� ����� �������� � �_� , ����� ��� �������
	// �������� �����  � ���� �������� ��� �������� ������ , � ������� ������ �������� ����� ����� ����� �������� ���� � ����� �����

	CardName Spr;
	string suit;
	string rang;
	switch (this->pack[number].suit) {
	case 0:
		suit = "H";
		break;
	case 1:
		suit = "D";
		break;
	case 2:
		suit = "C";
		break;
	case 3:
		suit = "S";
		break;
	default:
		break;
	}
	if (this->pack[number].rang < 9) {
		std::stringstream ss;
		ss << this->pack[number].rang + 2;
	
		ss >> rang;
	}
	else {
		switch (this->pack[number].rang) {
		case 9:
			rang = "J";
			break;
		case 10:
			rang = "Q";
			break;
		case 11:
			rang = "K";
			break;
		case 12:
			rang = "A";
			break;
		default:
			break;
		}
	}
	string goal = (suit + '_' + rang+".jpg");
	Spr.name = goal;
	Spr.rang = this->pack[number].rang;
	Spr.suit = this->pack[number].suit;
	return Spr;
}



int PlayingCards::operator ()(int nom)const
{
	Card tmp;
	if (nom <0 || nom > n - 1)
		throw "Actual number of cards in pack less than";
	else
		tmp = pack[nom];
	return tmp.suit;
}



int  PlayingCards :: operator [] (int nom)const
{
	Card tmp;
	if (nom <0 || nom > n - 1)
		throw "Actual number of cards in pack less than";
	else
		tmp = pack[nom];
	return tmp.rang;
}



PlayingCards& PlayingCards::regulatPack() {
	for (int i = 0; i<n; i++)
		for (int j = 0; j<n - 1; j++) {
			if (pack[i].suit>pack[j].suit)
			{
				int m, r;
				m = pack[i].suit;
				r = pack[i].rang;
				pack[i].suit = pack[j].suit;
				pack[i].rang = pack[j].rang;
				pack[j].suit = m;
				pack[j].rang = r;
			}
		}
	for (int i = 0; i<n; i++)
		for (int j = 0; j<n - 1; j++)
		{
			if (pack[i].suit == pack[j].suit && pack[i].rang>pack[j].rang)
			{
				int r;
				r = pack[i].rang;
				pack[i].rang = pack[j].rang;
				pack[j].rang = r;
			}
		}
	return *this;
}



int PlayingCards::findTheSame(int rang, int suit)
{
	for (int j = 0; j < n; j++)
		if (pack[j].suit == suit && pack[j].rang == rang)
			return 0;
	return 1;
}



PlayingCards PlayingCards::allotSub(int m)const {
	PlayingCards newpack;
	Card *buf;
	int  j = 0, fl = 0;
	if (m<0 || m > 3)
		throw "Illegal suit value";
	if (n > 13)
		buf = new Card[13];
	else
		buf = new Card[n];
	for (int i = 0; i<n; i++)
	{
		if (pack[i].suit == m)
		{
			fl = 1;
			newpack.n += 1;
			buf[j].suit = pack[i].suit;
			buf[j].rang = pack[i].rang;
			j++;
		}
	}
	if (!fl)
	{
		delete[] buf;
		throw "There is not this suit in pack";
	}
	else
	{
		newpack.pack = new Card[newpack.n];
		for (int i = 0; i < newpack.n; ++i)
			newpack.pack[i] = buf[i];
		delete[] buf;
	}
	return newpack;
}


PlayingCards & PlayingCards:: addCard(const Card &NewCard) {

	if (n + 1<SZ + 1)
	{
		Card *buf = pack;
		pack = new Card[n + 1];
		for (int i = 0; i < n; ++i)
			pack[i] = buf[i];
		pack[n] = NewCard;
		++n;
		delete[] buf;
	}
	else
		throw "Pack is full";

	return *this;
}
















//istream& operator >> (istream& in, PlayingCards& re) // ---- ???????? ?????????? (?????)
//{
//	int rang, suit, n, real = 0;
//	Card *buf;
//	in >> n;
//
//	if (n<1 || n>re.SZ)
//		in.setstate(in.failbit);
//	re.pack = new Card[n];
//	for (int i = 0; i < n; ++i)
//	{
//		in >> suit >> rang;
//		if (suit < 0 || suit>3)
//		{
//			in.setstate(in.failbit);
//			delete[] re.pack;
//			re.pack = nullptr;
//		}
//		if (rang < 0 || rang>12)
//		{
//			in.setstate(in.failbit);
//			delete[] re.pack;
//			re.pack = nullptr;
//		}
//		if (re.findTheSame(rang, suit))
//		{
//			re.pack[real].suit = suit;
//			re.pack[real].rang = rang;
//			++real;
//		}
//		re.n = real;
//		if (real != n)
//		{
//			buf = re.pack;
//			re.pack = new Card[real];
//			for (int i = 0; i < real; ++i)
//				re.pack[i] = buf[i];
//		}
//	}
//	return in;
//}
//

//
//PlayingCards & PlayingCards :: operator ++() {
	//
	//	if (n + 1<SZ + 1)
	//	{
	//		Card *buf = pack;
	//		pack = new Card[n + 1];
	//		for (int i = 0; i < n; ++i)
	//			pack[i] = buf[i];
	//		int suit, rang;
	//		do {
	//			suit = rand() % 4;
	//			rang = rand() % 13;
	//		} while (!findTheSame(rang, suit));
	//		pack[n].suit = suit;
	//		pack[n].rang = rang;
	//		++n;
	//		delete[] buf;
	//	}
	//	else
	//		throw "Pack is full";
	//
	//	return *this;
	//}


//
//ostream& operator <<(ostream& os, const PlayingCards& kol)
//{
//	int six = kol.n / 6, ost = kol.n % 6, dob = 0, res = 0;
//	if (ost == 0)
//		ost = 6;
//	else ++six;
//	os << kol.n << endl;
//	for (int k = 0; k < six; ++k) {
//		if (k + 1 != six)
//			dob = 6;
//		else dob = ost;
//		res += dob;
//		for (int i = 6 * k; i < res; i++)
//		{
//			if (kol.pack[i].rang >= 0 && kol.pack[i].rang < 9)
//				os << kol.pack[i].rang + 2 << "  ";
//			else
//				switch (kol.pack[i].rang) {
//				case 9:
//					os << "J  ";
//					break;
//				case 10:
//					os << "Q  ";
//					break;
//				case 11:
//					os << "K  ";
//					break;
//				case 12:
//					os << "A  ";
//					break;
//				}
//			char asciiChar = static_cast<char>(3 + kol.pack[i].suit);
//			os << asciiChar << "    ";
//		}
//		os << endl;
//		for (int i = 6 * k; i < res; i++)
//		{
//			char asciiChar = static_cast<char>(3 + kol.pack[i].suit);
//			os << asciiChar << "  ";
//			if (kol.pack[i].rang >= 0 && kol.pack[i].rang < 9)
//				os << kol.pack[i].rang + 2 << "    ";
//			else
//				switch (kol.pack[i].rang) {
//				case 9:
//					os << "J" << "    ";
//					break;
//				case 10:
//					os << "Q" << "    ";
//					break;
//				case 11:
//					os << "K" << "    ";
//					break;
//				case 12:
//					os << "A" << "    "; break;
//				}
//		}
//
//		os << endl << endl;
//	}
//
//	return os;
//}



	//PlayingCards & PlayingCards:: operator =(const PlayingCards &obj)
		//{
		//	if (this == &obj) return *this; // ???????? ?? ????????????????
		//	delete[] pack; // ??????????? ??????????? ????????
		//	pack = new Card[obj.n];
		//	n = obj.n;
		//	for (int i = 0; i < n; ++i)
		//		pack[i] = obj.pack[i];
		//	return *this;
		//}
		//
		//PlayingCards & PlayingCards:: operator =(PlayingCards &&obj)
		//{
		//	int tmp = n;
		//
		//	n = obj.n;
		//
		//	obj.n = tmp;
		//
		//	Card *ptr = pack;
		//
		//	pack = obj.pack;
		//
		//	obj.pack = ptr;
		//
		//	return *this;
		//}
		//
		////